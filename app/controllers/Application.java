package controllers;

import com.atlassian.connect.play.java.CheckValidOAuthRequest;
import com.atlassian.connect.play.java.controllers.AcController;
import com.google.common.base.Supplier;
import play.*;
import play.mvc.*;

import views.html.*;
import views.xml.*;

public class Application extends Controller {
    public static Result index() {
        return AcController.index(
                descriptor(), // we keep the HTML home page from the module, as this is our documentation for now
                descriptor());                // serve the descriptor when accept header is 'application/xml', try 'curl -H "Accept: application/xml" http://localhost:9000'
    }

    private static Supplier descriptor() {
        return new Supplier() {
            @Override
            public Result get() {
                return ok(descriptor.render());
            }
        };
    }

    @CheckValidOAuthRequest
    public static Result demo() {
        Logger.info(request().uri());
        return Results.ok(demo.render());
    }

    @CheckValidOAuthRequest
    public static Result demoEditor() {
        return ok(demoEditor.render());
    }
}
